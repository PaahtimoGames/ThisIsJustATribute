class_name AbilityBarUIComponent
extends Control
@export var pg_actions : Array[PGAction]

const SPELL_CURSOR = preload("res://art/spell_cursor.png")

func update(_delta) -> bool: 

	if Input.is_action_just_pressed("interact") and EventBus.mouse_casting and EventBus.current_interactor != null:
		var interactor = EventBus.current_interactor.get_parent()
		for pgaction in pg_actions:
			pgaction.init(interactor)
			pgaction.enter()
			
		EventBus.mouse_casting = false
		return true
	return false
func _on_ability_1_button_pressed():
	print("Ability1 pressed.")
	EventBus.mouse_casting = true
	EventBus.current_interactor = null
	Input.set_custom_mouse_cursor(SPELL_CURSOR)
	
func _on_ability_1_button_mouse_entered():
	EventBus.mouse_over += 1


func _on_ability_1_button_mouse_exited():
	EventBus.mouse_over -= 1
