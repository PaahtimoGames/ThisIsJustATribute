class_name AIComponent
extends Node

const WHITE_SPRITE_MATERIAL = preload("res://art/white_sprite_material.tres")
const GORE_SPLAT_L_GROUND_01_MONO = preload("res://art/GORE_Splat_L_Ground_01_mono.wav")
const GORE_STAB_01_MONO = preload("res://art/GORE_Stab_01_mono.wav")
const BLOOD_EFFECT = preload("res://scenes/particles/blood_effect.tscn")
const COLLECTABLE = preload("res://scenes/collectable/collectable.tscn")
const AXE_ITEM = preload("res://scenes/enemy/axe_item.tscn")

@export var speed = 150
@export var attack_component : AttackComponent
@export var move_distance := 16
@export var health := 3
@export var ray_cast_2d : RayCast2D
@export var sprite_2d : Sprite2D
@export var max_turns := 2
@export var target_range := 10
@export var parent_node : Node2D
@export var attack_actions : Array[PGAction]
var target : NodeContainer
var tween : Tween
var enemy_took_action := 1
var facing_direction := Vector2.ZERO

var turn_timer = 2
var turn_timer_counter = 0
var destination : Vector2

func _ready():
	#alert.hide()
	enemy_took_action = max_turns
	EventBus.tile_made_walkable.emit(parent_node.global_position,false)
	target = EventBus.player
	destination = parent_node.position
		
#func _process(delta):
	#turn_timer_counter -= delta
	#if turn_timer_counter <= 0:
		#update()
		#turn_timer_counter = turn_timer
		
func reset_turns():
	enemy_took_action = max_turns
			
func set_target(new_target : Player):
	target = new_target
	
func is_dead() -> bool:
	return health <= 0	
	
		
func update(_delta) -> bool:		
	facing_direction = Vector2.ZERO

	var range_to_target = 0
	if target:
		range_to_target = (parent_node.position.distance_to(target.position)) /16
		
	if target and range_to_target <= target_range:
		#alert.show()
		facing_direction = move_to_target()
	else:
		#alert.hide()
		facing_direction = random_movement()
		
	if attack_component and target and range_to_target <= 1.5:
		attack_component.enable_attack_area(facing_direction)
		enemy_took_action -= max_turns
		ray_cast_2d.enabled = false
		return true
	elif ray_cast_2d.is_colliding():
		var raytarget = ray_cast_2d.get_collider()
			
		if not raytarget.is_in_group("enemy"):
			enemy_took_action -= max_turns
			ray_cast_2d.enabled = false
			return true

	ray_cast_2d.enabled = false
	enemy_took_action -= 1
	EventBus.tile_made_walkable.emit(parent_node.global_position,true)
	destination += facing_direction
	EventBus.tile_made_walkable.emit(destination,false)
	return true
	
func _physics_process(delta):
	var range_to_target = (parent_node.position.distance_to(destination))	
	if range_to_target > 0.1: 
		parent_node.position = parent_node.position.move_toward(destination, delta * speed)
		
func move_to_target()-> Vector2:
	var direction = Vector2.ZERO
				
	if target.position.x > parent_node.position.x:
		direction.x += move_distance
		set_raycast_direction(Vector2(16,8), Vector2(move_distance,0))
	elif target.position.x < parent_node.position.x:
		direction.x -= move_distance
		set_raycast_direction(Vector2(0,8), Vector2(-move_distance,0))
	elif target.position.y < parent_node.position.y:
		direction.y -= move_distance
		set_raycast_direction(Vector2(8,0), Vector2(0,-move_distance))
	elif target.position.y > parent_node.position.y:
		direction.y += move_distance
		set_raycast_direction(Vector2(8,16), Vector2(0,move_distance))
		
		
	return direction
	
func random_movement() -> Vector2:
	var direction = Vector2.ZERO
	var input = randi_range(0,3)
	
	if input == 0:
		direction.x += move_distance
		set_raycast_direction(Vector2(17,8), Vector2(move_distance,0))
	elif input == 1:
		direction.x -= move_distance
		set_raycast_direction(Vector2(-1,8), Vector2(-move_distance,0))
	elif input == 2:
		direction.y -= move_distance
		set_raycast_direction(Vector2(8,1), Vector2(0,-move_distance))
	elif input == 3:
		direction.y += move_distance
		set_raycast_direction(Vector2(8,17), Vector2(0,move_distance))

	return direction
		
		
func set_raycast_direction(ray_position: Vector2,direction : Vector2):
	ray_cast_2d.target_position = direction
	ray_cast_2d.position = ray_position
	ray_cast_2d.force_raycast_update()
	ray_cast_2d.enabled = false


