class_name AttackComponent
extends Area2D

@export var collision_shape : CollisionShape2D
@export var base_position := Vector2(8,8)
@export var attack_actions : Array[PGAction]
@export var parent_node : Node2D
func _ready():
	collision_shape.disabled = true

func enable_attack_area(new_position : Vector2):
	collision_shape.position = base_position + new_position
	collision_shape.disabled = false
	
func disable_attack_area():
	collision_shape.position = base_position
	collision_shape.set_deferred("disabled", true)

func _on_area_entered(area):
	for child in area.get_parent().get_children():
		if child is HealthComponent:
			var hit = randi_range(1,20)
			var damage = randi_range(1,6)
			child.take_damage(hit,damage)
			break
	disable_attack_area()


func _on_area_exited(_area):
	pass
