extends Camera2D

@export var camera_target : Node2D

func _ready():
	if camera_target:
		position = camera_target.position
		
func set_target(new_target):
	camera_target = new_target

	if camera_target:
		position = camera_target.position
		
		
func _physics_process(_delta):
	if not camera_target:
		return
		
	position = lerp(position, camera_target.position, 0.08)
