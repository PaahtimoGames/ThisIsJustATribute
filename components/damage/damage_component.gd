class_name DamageComponent
extends Node

@export var hope_component : HopeComponent

func roll_attack()-> int:
	var dice_roll = hope_component.roll_dice()
	return dice_roll
	
func get_damage() -> int:
	var damage = randi_range(1,6)
	return damage
