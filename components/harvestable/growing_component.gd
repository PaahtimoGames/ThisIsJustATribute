class_name GrowingComponent
extends Node


@export var new_sprite : Texture
@export var sprite_2d : Sprite2D

var fully_grown := false
func _ready():
	await get_tree().create_timer(2).timeout
	sprite_2d.texture = new_sprite
	fully_grown = true
