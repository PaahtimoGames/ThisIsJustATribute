class_name HarvestableComponent
extends Node

@export var harvest_items : Array[String]
@export var empty_texture : Texture
@export var sprite_2d : Sprite2D
var dirt : Dirt

var item_harvested := false

func items_harvested():
	item_harvested = true
	if sprite_2d == null:
		# quick hack
		get_parent().hide()
		await get_tree().create_timer(0.1).timeout
		get_parent().queue_free()
		return
	sprite_2d.texture = empty_texture
	
func get_items():
	if item_harvested:
		return []
	return harvest_items
