class_name HealthComponent
extends Node
const GORE_SPLAT_L_GROUND_01_MONO = preload("res://art/GORE_Splat_L_Ground_01_mono.wav")
const GORE_STAB_01_MONO = preload("res://art/GORE_Stab_01_mono.wav")
const WHITE_SPRITE_MATERIAL = preload("res://art/white_sprite_material.tres")
@export var max_health := 10
@export var evasion := 10
@export var sprite_2d : Sprite2D
@export var parent_node : Node2D
@export var death_events : Array[PGAction] = []

var health := 2
var tween : Tween

func _ready():
	health = max_health

func update_max_health(new_health : int):
	max_health = new_health
	health = max_health


func take_damage(hit_roll : int, damage : int) -> bool:

	tween = create_tween()

	tween.tween_interval(0.17)
	
	if hit_roll >= evasion:
		health -= damage
		health = clampi(health,0,max_health)
		tween.tween_callback(Shaker.shake.bind(parent_node,10,0.1))
		sprite_2d.material = WHITE_SPRITE_MATERIAL
		SfxPlayer.play(GORE_SPLAT_L_GROUND_01_MONO)
	else:
		tween.tween_callback(Shaker.shake.bind(parent_node,5,0.05))
		SfxPlayer.play(GORE_STAB_01_MONO)
	tween.finished.connect(
		func():
			sprite_2d.material = null
			if health <= 0:
				for pgaction in death_events:
					pgaction.init(parent_node)
					pgaction.enter()

	)
	return hit_roll >= evasion

