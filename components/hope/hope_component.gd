class_name HopeComponent
extends Node
signal hope_updated
@export var max_hope := 5
@export var min_hope := 0
var hope := 0

func set_hope(value : int):
	hope = clamp(value, min_hope, max_hope)
	hope_updated.emit()

func roll_dice() -> int:
	var hope_roll = randi_range(1,12)
	var fear_roll = randi_range(1,12)
	if hope_roll >= fear_roll:
		hope += 1
	EventBus.roll_hope_and_fear_dice.emit(hope_roll, fear_roll)
	return hope_roll + fear_roll
		
func set_roll_text(text : String):
	EventBus.set_roll_result_text.emit(text)
