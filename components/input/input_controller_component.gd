extends Node

var inventory_enabled = false
@export var inventory : InventoryUI
	
func _process(_delta):	
	if Input.is_action_just_pressed("show_menu") and not inventory_enabled:
		enable_inventory()
	elif Input.is_action_just_pressed("show_menu") and inventory_enabled:
		disable_inventory()
		
func enable_inventory():
	inventory.set_process(true)
	inventory.show()
	inventory_enabled = true
	
func disable_inventory():
	inventory.set_process(false)
	inventory.hide()
	inventory_enabled = false
