extends PGAction

@export var item : String
var interaction_component : InteractionComponent
var parent_node : Node
			
func init(node : Node):
	parent_node = node
	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
			
func enter():

				
	var interactor = interaction_component.interactor
	var inventory :InventoryComponent

	for child in interactor.get_children():
		if child is InventoryComponent:
			inventory = child
	
		
	inventory.pickup_item(item)
