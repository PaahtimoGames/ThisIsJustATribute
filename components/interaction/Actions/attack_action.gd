extends PGAction

var interaction_component : InteractionComponent
var health_component : HealthComponent
			
func init(node : Node):

	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
		if child is HealthComponent:
			health_component = child
			
func enter():			
	var interactor = interaction_component.interactor
	var damage_component :DamageComponent
	var hope_component : HopeComponent
	for child in interactor.get_children():
		if child is DamageComponent:
			damage_component = child
		if child is HopeComponent:
			hope_component = child
		
	var attack_roll = damage_component.roll_attack()
	var damage_roll = damage_component.get_damage()			
	var attack_success = health_component.take_damage(attack_roll, damage_roll)
	if attack_success:			
		var text = "Dealt %s damage." % damage_roll
		hope_component.set_roll_text(text)
	else:
		var text = "Attack missed."
		hope_component.set_roll_text(text)
