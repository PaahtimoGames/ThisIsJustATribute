extends PGAction

var interaction_component : InteractionComponent
var inventory_component : InventoryComponent
			
func init(node : Node):

	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
		if child is InventoryComponent:
			inventory_component = child
func enter():	
		
	var interactor = interaction_component.interactor
	var inventory :InventoryComponent
	for child in interactor.get_children():
		if child is InventoryComponent:
			inventory = child

	inventory.seeds += 1
	inventory_component.seeds -= 1

