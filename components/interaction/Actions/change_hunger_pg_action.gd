extends PGAction

var parent : ResourceComponent

@export var value : int

func init(node : Node):
	for child in node.get_children():
		if child is ResourceComponent:
			parent = child
			break
			

func enter():
	parent.hunger += value
