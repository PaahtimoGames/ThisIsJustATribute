extends PGAction

var parent : InventoryComponent
var hope_component : HopeComponent

@export var value : int
@export var item_name : String


func init(node : Node):
	for child in node.get_children():
		if child is InventoryComponent:
			parent = child
		if child is HopeComponent:
			hope_component = child
			

func enter():
	parent.coin -= value
	if item_name.length() > 0:
		var dice_roll = hope_component.roll_dice()
		if dice_roll > 20:			
			parent.pickup_item(item_name)
		if dice_roll > 10:			
			parent.pickup_item(item_name)
			
		var text = "%s collected." % item_name
		hope_component.set_roll_text(text)
