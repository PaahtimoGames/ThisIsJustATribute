extends PGAction

enum ConditionType {ALL_TRUE,ANY_TRUE}
@export var condition_type : ConditionType
@export var conditions :Array[PGAction]
@export var actions :Array[PGAction]

var action_node : Node

func init(node : Node):
	action_node = node
			
func enter():
	if !conditions_are_true():
		return
	for pgaction in actions:
		var action = pgaction.duplicate()
		action.init(action_node)
		action.enter()

func is_valid() -> bool:
	return conditions_are_true()

func conditions_are_true() -> bool:
	for condition in conditions:
		var action = condition.duplicate()
		action.init(action_node)
		var is_true = action.is_valid() 
		if is_true and condition_type == ConditionType.ANY_TRUE:
			return true
		elif !is_true and condition_type == ConditionType.ALL_TRUE:
			return false
	return condition_type == ConditionType.ALL_TRUE
