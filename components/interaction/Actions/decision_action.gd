extends PGAction

enum ConditionType {ALL_TRUE,ANY_TRUE}
@export var condition_type : ConditionType
@export var conditions :Array[PGAction]

var action_node : Node

func init(node : Node):
	action_node = node
			

func is_valid() -> bool:
	for condition in conditions:
		var action = condition.duplicate()
		action.init(action_node)
		var is_true = action.is_valid() 
		if is_true and condition_type == ConditionType.ANY_TRUE:
			return true
		elif !is_true and condition_type == ConditionType.ALL_TRUE:
			return false
	return condition_type == ConditionType.ALL_TRUE
