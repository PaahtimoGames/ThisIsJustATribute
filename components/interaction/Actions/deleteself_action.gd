extends PGAction

var parent_node : Node
			
func init(node : Node):
	parent_node = node
	
func enter():	
	# quick hack
	parent_node.hide()
	await parent_node.get_tree().create_timer(0.1).timeout
	if parent_node != null:
		parent_node.queue_free()

