extends PGAction
enum ConditionType {ALL_TRUE,ANY_TRUE}
@export var condition_type : ConditionType
@export var conditions :Array[PGAction]
@export var actions :Array[PGAction]
var interaction_component : InteractionComponent

func init(node : Node):
	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
			break
func enter():
	var interactor = interaction_component.interactor
	for pgaction in actions:
		var action = pgaction.duplicate()
		action.init(interactor)
		action.enter()

func is_valid() -> bool:
	var interactor = interaction_component.interactor
	if interactor == null:
		return false
	for condition in conditions:
		var action = condition.duplicate()
		action.init(interactor)
		var is_true = action.is_valid() 
		if is_true and condition_type == ConditionType.ANY_TRUE:
			return true
		elif !is_true and condition_type == ConditionType.ALL_TRUE:
			return false
	return condition_type == ConditionType.ALL_TRUE
