extends PGAction
var harvestable_component : HarvestableComponent
var interaction_component : InteractionComponent
var growing_component : GrowingComponent
var parent_node : Node
			
func init(node : Node):
	parent_node = node
	for child in node.get_children():
		if child is HarvestableComponent:
			harvestable_component = child
		if child is InteractionComponent:
			interaction_component = child
		if child is GrowingComponent:
			growing_component = child
			
func enter():
	if harvestable_component.dirt:
		harvestable_component.dirt.enable_interaction()
				
	var interactor = interaction_component.interactor
	var inventory :InventoryComponent
	var hope_component : HopeComponent
	for child in interactor.get_children():
		if child is InventoryComponent:
			inventory = child
		if child is HopeComponent:
			hope_component = child
	var items = harvestable_component.get_items()					
		
	if !growing_component or growing_component.fully_grown:		
		for item in items:
			var dice_roll = hope_component.roll_dice()
			if dice_roll > 20:			
				inventory.pickup_item(item)
			if dice_roll > 10:			
				inventory.pickup_item(item)	
				
			var text = "%s collected." % item
			hope_component.set_roll_text(text)
		
	harvestable_component.items_harvested()
