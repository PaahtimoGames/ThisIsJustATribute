extends PGAction

var parent : InventoryComponent

@export var value : int
func init(node : Node):
	for child in node.get_children():
		if child is InventoryComponent:
			parent = child
			break
			

func is_valid() -> bool:
	return parent.coin >= value
