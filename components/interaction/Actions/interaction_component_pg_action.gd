extends PGAction

@export var allow_interaction := true
var interaction_component : InteractionComponent

func init(node : Node):
	interaction_component = node as InteractionComponent

func enter():
	interaction_component.interactable = allow_interaction
