extends PGAction
var harvestable_component : HarvestableComponent
			
func init(node : Node):

	for child in node.get_children():
		if child is HarvestableComponent:
			harvestable_component = child
			
func is_valid() -> bool:			
	return !harvestable_component.item_harvested

