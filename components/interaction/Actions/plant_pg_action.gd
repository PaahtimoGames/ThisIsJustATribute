extends PGAction

@export var plant := PackedScene
var interaction_component : InteractionComponent
var parent : Node
func init(node : Node):
	parent = node
	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
func enter():
	var spawn_position = Vector2.ZERO
	if parent:
		spawn_position = parent.global_position
		
	var inventory :InventoryComponent
	for child in interaction_component.interactor.get_children():
		if child is InventoryComponent:
			inventory = child
	inventory.seeds -= 1
	EventBus.spawn_something.emit(plant,spawn_position,after_spawn_call)

func after_spawn_call(spawned_plant : Node):
	for child in spawned_plant.get_children():
		if child is HarvestableComponent:
			child.dirt = parent
			break
	var dirt = parent as Dirt
	dirt.disable_interaction()
