extends PGAction

@export var move_distance := 16

var interaction_component : InteractionComponent
var health_component : HealthComponent
var ray_cast_2d : RayCast2D	
var ai_component : AIComponent	

func init(node : Node):

	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
		if child is AIComponent:
			ai_component = child
			ray_cast_2d = child.ray_cast_2d
			
func enter():		
	if !ray_cast_2d.is_colliding():
		return
		
	update_direction(ai_component.facing_direction)	
			
	var raytarget = ray_cast_2d.get_collider()
	if raytarget and raytarget.is_in_group("player"):
		var hit = randi_range(1,20)
		var damage = randi_range(1,6)
		
		for child in raytarget.get_parent().get_children():
			if child is HealthComponent:
				child.take_damage(hit,damage)

		
func update_direction(position : Vector2)-> Vector2:
	var direction = Vector2.ZERO
				
	if position.x > 0:
		set_raycast_direction(Vector2(16,8), Vector2(move_distance,0))
	elif position.x < 0:
		set_raycast_direction(Vector2(0,8), Vector2(-move_distance,0))
	elif position.y < 0:
		set_raycast_direction(Vector2(8,0), Vector2(0,-move_distance))
	elif position.y > 0:
		set_raycast_direction(Vector2(8,16), Vector2(0,move_distance))
		
	return direction

func set_raycast_direction(ray_position: Vector2,direction : Vector2):
	ray_cast_2d.target_position = direction
	ray_cast_2d.position = ray_position
	ray_cast_2d.force_raycast_update()
	ray_cast_2d.enabled = true

