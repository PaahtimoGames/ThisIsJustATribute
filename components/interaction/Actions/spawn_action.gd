extends PGAction

@export var spawn_actions : Array[PGAction]
@export var object := PackedScene

var parent : Node
func init(node : Node):
	parent = node

func enter():

	EventBus.spawn_something.emit(object,parent.position,after_spawn_call)

func after_spawn_call(spawned_plant : Node):
	for action in spawn_actions:
		action.init(spawned_plant)
		action.enter()
