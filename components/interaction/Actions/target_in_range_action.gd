extends PGAction

@export var range_required := 1.5
var interaction_component : InteractionComponent
var parent_node : Node2D
var node_size := 16	
			
func init(node : Node):
	parent_node = node as Node2D
	for child in node.get_children():
		if child is InteractionComponent:
			interaction_component = child
			
func is_valid() -> bool:		
	var interactor = interaction_component.interactor
	if interactor == null:
		return false
	var range_to_target = (parent_node.position.distance_to(interactor.position)) /node_size
	
	var is_in_range = range_to_target <= range_required
	return is_in_range
