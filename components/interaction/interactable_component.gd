class_name InteractionComponent
extends Area2D
const OUTLINE_MATERIAL = preload("res://art/outline_material.tres")
@export var interaction_data : Array[InteractionData]
@export var interaction_description := "Interactable"

@onready var interaction_ui = %InteractionUI
@onready var button_container = %ButtonContainer
@onready var hope_text = %HopeText
@onready var fear_text = %FearText
@onready var roll_container = %RollContainer
@onready var roll_description = %RollDescription

var interactable = true
@export var moving := false
var interactor : Node
@export var cursor_texture : Texture
@export var sprite_2d : Sprite2D
@export var dicecontainer_sfa := false
var show_time := 0.0
var max_showtime = 1.0

var interacted := false

func _ready():
	hide_ui()
	EventBus.interaction_ui_disable.connect(hide_ui)
	if !moving:
		EventBus.tile_made_walkable.emit(global_position,false)
		
	if dicecontainer_sfa:
		EventBus.roll_hope_and_fear_dice.connect(roll_create_rolls)
		EventBus.set_roll_result_text.connect(set_roll_description_text)

func _exit_tree():
	if !moving:
		EventBus.tile_made_walkable.emit(global_position,true)
		
func roll_create_rolls(hope_roll : int, fear_roll : int):
	roll_container.show()
	interaction_ui.show()
	hope_text.text = str(hope_roll)
	fear_text.text = str(fear_roll)
	
	show_time = max_showtime
	#get_tree().create_timer(1,false).timeout.connect(
		#func():
			#roll_container.hide()
			#interaction_ui.hide()
	#)	
func set_roll_description_text(text: String):
	roll_description.text = text
	
func create_buttons():
	var parent = get_parent()
	for interaction in interaction_data:
		var button = interaction.button_prefab.instantiate()
		var button_actions : Array[PGAction] = []
		for pg_action in interaction.pg_action:
			var action = pg_action.duplicate()
			action.init(parent)
			#is_valid = is_valid && action.is_valid()
			button.connect("pressed", action.enter.bind())
			button_actions.append(action)
		button.actions = button_actions
		button.connect("pressed", interaction_ui.hide.bind())
		button.set_text(interaction.button_text)
		button.update_tooltip(interaction.button_tooltip)
		button_container.add_child(button)

func _process(_delta):
	if  show_time > 0:
		show_time -= _delta
		if show_time <= 0:
			roll_container.hide()
			interaction_ui.hide()
	if Input.is_action_pressed("uninteract") and EventBus.mouse_over <= 0:
		interaction_ui.hide()
		
func _on_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("interact") and interactable and interaction_data.size() > 0:
		EventBus.current_interactor = self
		interactor = EventBus.player
		if !EventBus.mouse_casting:
			var parent = get_parent()
			var moment_component :MovementComponent
			for child in interactor.get_children():
				if child is MovementComponent:
					moment_component = child
					moment_component.set_new_destination(parent.position,call_first_action)
					break
				
		
	if event.is_action_pressed("uninteract") and interactable:
		interactor = EventBus.player
		EventBus.interaction_ui_disable.emit()
		for button in button_container.get_children():
			button.queue_free()
		create_buttons()
		interaction_ui.show()

func call_first_action():
	var parent = get_parent()
	var interaction = interaction_data[0]
	for pg_action in interaction.pg_action:
		var action = pg_action.duplicate()
		action.init(parent)
		action.enter()

func hide_ui():
	interaction_ui.hide()
		
func _on_mouse_entered():
	EventBus.mouse_over += 1
	if cursor_texture != null and interactable and !EventBus.mouse_casting:
		Input.set_custom_mouse_cursor(cursor_texture)
	if interactable:
		sprite_2d.material = OUTLINE_MATERIAL

func _on_mouse_exited():
	EventBus.mouse_over -= 1
	if cursor_texture != null and interactable and !EventBus.mouse_casting:
		Input.set_custom_mouse_cursor(null)
	sprite_2d.material = null

func _on_interaction_ui_mouse_entered():
	EventBus.mouse_over += 1


func _on_interaction_ui_mouse_exited():
	EventBus.mouse_over -= 1
