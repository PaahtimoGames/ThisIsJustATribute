class_name InteractionData
extends Resource

@export var button_text : String
@export var button_tooltip : String
@export var pg_action : Array[PGAction]
@export var button_prefab := preload("res://components/interaction/button_prefab.tscn")
