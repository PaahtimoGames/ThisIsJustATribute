class_name InteractionButton
extends Button

@export var label :Label

var actions : Array[PGAction]
var mouse_exit_timer_max := 0.1
var mouse_exit_timer := 0.0
func _ready():
	label.hide()
	
func _process(_delta):		
	if mouse_exit_timer > 0:
		mouse_exit_timer -= _delta
		if mouse_exit_timer <= 0:
			EventBus.mouse_over -= 1
			label.hide()
			
	for action in actions:
		if !action.is_valid():
			set_active(false)
			return
	set_active(true)

			
func update_tooltip(new_text : String):
	label.text = new_text

func set_active(value : bool):
	disabled = !value

func _on_mouse_entered():
	EventBus.mouse_over += 1
	if disabled:
		label.show()

func _on_mouse_exited():
	mouse_exit_timer = mouse_exit_timer_max

