class_name PGAction
extends Resource

@export var reverse := false

func init(_node : Node):
	pass
	
func enter():
	pass

func is_valid() -> bool:
	return !reverse
