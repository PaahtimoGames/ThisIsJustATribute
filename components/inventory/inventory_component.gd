class_name InventoryComponent
extends Node

@export var seeds := 0
@export var coin := 3
signal pickup_item_signal(item_id:String)

func pickup_item(item_id:String):
	pickup_item_signal.emit(item_id)
