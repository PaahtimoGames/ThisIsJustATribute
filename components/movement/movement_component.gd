class_name MovementComponent
extends Node


var destination = Vector2() 
@export var speed = 50
@export var player : NodeContainer
@export var movement_node : Node2D

var actions : Callable

var current_path : Array[Vector2i]
var current_path_index := 1
func get_ready():
	destination = movement_node.position
	
	
func _process(_delta): 
	if Input.is_action_just_pressed("interact") and EventBus.mouse_over <= 0 and !EventBus.mouse_casting:	
		EventBus.interaction_ui_disable.emit()
		var cursor_position = player.camera.get_global_mouse_position()
		var x_position = roundi((cursor_position.x -8) / 16) * 16
		var y_position = roundi((cursor_position.y -8)/ 16) * 16
		cursor_position = Vector2(x_position,y_position)
		set_new_destination(cursor_position, Callable())
	if Input.is_action_just_pressed("uninteract") and EventBus.mouse_casting:
		EventBus.mouse_casting = false
		Input.set_custom_mouse_cursor(null)

func update(_delta) -> bool: 
	if current_path_index >= current_path.size():
		if !actions.is_null():
			actions.call()
			actions = Callable()
			return true
		else:
			return false
	
	if movement_node.position == destination:
		current_path_index += 1
		if current_path.size() > current_path_index:
			destination = current_path[current_path_index] *16 as Vector2
		else:
			if !actions.is_null():
				actions.call()
				actions = Callable()
		return true
	return false

func _physics_process(delta):
	if current_path_index >= current_path.size():
		return
	
	var range_to_target = (movement_node.position.distance_to(destination))
	
	if range_to_target > 0.1: 
		movement_node.position = movement_node.position.move_toward(destination, delta * speed)

					
func set_new_destination(new_destination : Vector2,_function:Callable):
	#destination = new_destination
	move_with_pathfinding(movement_node.global_position,new_destination)
	actions = _function
	#if current_path_index >= current_path.size():
		#if !actions.is_null():
			#actions.call()
			#actions = Callable()

	
func move_with_pathfinding(current_position: Vector2, target_position : Vector2):

	var current_position_in_grid_x = roundi((current_position.x) / 16)
	var current_position_in_grid_Y = roundi((current_position.y)/ 16)
	var target_position_in_grid_x = roundi((target_position.x) / 16)
	var target_position_in_grid_Y = roundi((target_position.y)/ 16)
	
	var start_position = Vector2(current_position_in_grid_x, current_position_in_grid_Y)
	var end_position = Vector2(target_position_in_grid_x,target_position_in_grid_Y)
	
	var max_counter = 10
	var counter = 1
	var tilemap = player.tilemap
	if tilemap.astar_grid.is_point_solid(end_position):
		var arrays : Array[Vector2] = []
		while max_counter > counter:
			var new_end = end_position + Vector2(counter,0)
			if !tilemap.astar_grid.is_point_solid(new_end):
				arrays.append(new_end)
			new_end = end_position + Vector2(-counter,0)
			if !tilemap.astar_grid.is_point_solid(new_end):
				arrays.append(new_end)
			new_end = end_position + Vector2(0,counter)
			if !tilemap.astar_grid.is_point_solid(new_end):
				arrays.append(new_end)
			new_end = end_position + Vector2(0,-counter)
			if !tilemap.astar_grid.is_point_solid(new_end):
				arrays.append(new_end)

			if arrays.size() > 0:
				break		
			counter +=1
			
		var distance_to_end = 	(start_position.distance_to(end_position))
		for new_point in arrays:
			var distance = (start_position.distance_to(new_point))
			if distance < distance_to_end:
				end_position = new_point
				distance_to_end = distance
							
	current_path = tilemap.astar_grid.get_id_path(start_position,end_position)
	current_path_index = 1
	if current_path.size() > current_path_index:
		destination = current_path[current_path_index] *16 as Vector2
	

