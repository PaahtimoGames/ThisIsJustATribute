class_name NodeContainer
extends Node2D
@export var tilemap : TileMap
@export var camera : Camera2D
@export var child_nodes : Array[Node]
var turn_completed := false
@export var is_player := false

func _ready():
	if is_player:
		EventBus.player = self


func play_turn(_delta):
	var all_objects_ready = true
	for child in child_nodes:
		var child_ready = child.update(_delta)
		if child_ready:
			turn_completed = true	
	
		
func start_turn():
	turn_completed = false
	
func end_turn():
	pass
