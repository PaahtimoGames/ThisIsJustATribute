extends ProgressBar


@export var resource_component : ResourceComponent

signal resource_update


func _ready():
	max_value = resource_component.max_hunger
	value = resource_component.hunger
	resource_component.resource_update.connect(update_bar)
	
func update_bar():
	value = resource_component.hunger
