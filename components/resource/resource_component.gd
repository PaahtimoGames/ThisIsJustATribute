class_name ResourceComponent
extends Node

signal resource_update

var max_hunger := 100
var hunger : set = set_hunger

var hunger_counter = 60
var hunger_counter_max = 60
func _ready():
	hunger = max_hunger
	
	
func update(_delta):
	hunger_counter -= 1
	if hunger_counter <= 0:
		hunger -= 1
		hunger_counter = hunger_counter_max
		
	if hunger <= 0:
		SceneManager.load_gameoverscene()
	
	
func set_hunger(value : int):
	hunger = value
	hunger = clamp(hunger,0,max_hunger)
	resource_update.emit()
