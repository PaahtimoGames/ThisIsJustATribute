extends Node

@export var run_stats : RunStats
@export var health_component : HealthComponent
@export var sprite_2d : Sprite2D
@export var character_stats : CharacterStats
func _ready():
	character_stats = run_stats.character_stats
	print(character_stats)
	health_component.update_max_health(character_stats.max_health)
	sprite_2d.texture = character_stats.portrait

