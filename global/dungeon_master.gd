extends Node


var hope := 0

func roll_hope_and_fear() -> Array:
	var hope_roll = randi_range(1,12) 
	var fear_roll = randi_range(1,12)
	EventBus.roll_hope_and_fear_dice.emit(hope_roll,fear_roll)
	var total = fear_roll + hope_roll
	var roll_type = 0
	if fear_roll > hope_roll:
		EventBus.fear_updated.emit(1)
		roll_type = -1
	elif hope_roll > fear_roll:
		roll_type = 1
	
	return [total, roll_type]
