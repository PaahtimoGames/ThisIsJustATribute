extends Node

var player : NodeContainer
var current_interactor : InteractionComponent
var mouse_over := 0 : set = set_value 
var mouse_casting := false
func set_value(value :int):
	mouse_over = value

signal spawn_something(node : Object, spawn_position : Vector2, _function)
signal attack_event(node : Node2D)
signal text_block_added(new_text : String)

signal health_updated(value : int)

signal something_clicked(node : Node2D, text : String)

signal roll_hope_and_fear_dice(hope_roll: int,fear_roll: int)
signal set_roll_result_text(text : String)

signal hope_updated(value : int)
signal fear_updated(value : int)
signal coin_updated(value : int)

signal player_button_clicked(index : int)

signal tile_made_walkable(position : Vector2, value : bool)
signal fireball_pressed()
signal pickup_item(item_name:String)

signal interaction_ui_disable()
