extends Node

const MAIN_MENU = preload("res://scenes/main_menu/main_menu.tscn")
const FARM_SCENE = preload("res://scenes/farm_scene.tscn")
const GAME_OVER = preload("res://scenes/main_menu/game_over.tscn")
const CHARACTER_SELECTOR = preload("res://scenes/character_select/character_selector.tscn")

func load_character_selector():
	get_tree().change_scene_to_packed(CHARACTER_SELECTOR)
	
func load_mainmenu():
	get_tree().change_scene_to_packed(MAIN_MENU)
	EventBus.mouse_over = 0
func load_runscene():
	get_tree().change_scene_to_packed(FARM_SCENE)
	EventBus.mouse_over = 0
	
func load_gameoverscene():
	get_tree().change_scene_to_packed(GAME_OVER)

func load_scene(scene : PackedScene):
	var scene_tree = get_tree()
	for tween in scene_tree.get_processed_tweens():
		tween.kill()
	scene_tree.change_scene_to_packed(scene)
