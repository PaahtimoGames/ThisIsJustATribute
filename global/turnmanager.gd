extends Node

@export var characters : Array[Node]
var active_character
var current_index := 0
func _ready():
	active_character = characters[0]
	
func _process(delta):
	if active_character == null:
		characters.remove_at(current_index)
		if current_index >= characters.size():
			current_index = 0
		active_character = characters[current_index]
	
	if !active_character.turn_completed:
		active_character.play_turn(delta)
	else:
		active_character.end_turn()
		current_index += 1
		if current_index >= characters.size():
			current_index = 0
		active_character = characters[current_index]
		if active_character != null:
			active_character.start_turn()
		else:
			characters.remove_at(current_index)
			if current_index >= characters.size():
				current_index = 0
			active_character = characters[current_index]
