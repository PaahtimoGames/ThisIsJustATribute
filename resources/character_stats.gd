class_name CharacterStats
extends Resource

@export var name :String
@export var description :String
@export var portrait :Texture2D
@export var max_health := 10
