extends Control

@onready var ability_container = %AbilityContainer
const ABILITY_BOX = preload("res://scenes/ability_box.tscn")

func create_containers(player : Player):
	for child in ability_container.get_children():
		child.queue_free()

	for ability in player.abilities:
		var ability_box = ABILITY_BOX.instantiate() as AbilityBox
		ability_box.set_data(ability)
		ability_container.add_child(ability_box)
