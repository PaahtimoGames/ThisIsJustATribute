class_name AbilityBox
extends Control
@export var icon :TextureRect

func set_data(ability : PlayerAbility):
	icon.texture = ability.texture

func _on_button_pressed():
	EventBus.fireball_pressed.emit()
