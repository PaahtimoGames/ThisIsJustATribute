extends Control

const FARM_SCENE = preload("res://scenes/farm_scene.tscn")
const ROGUE_CHARACTER_STATS = preload("res://resources/rogue_character_stats.tres")
const WARRIOR_CHARACTER_STATS_2 = preload("res://resources/warrior_character_stats_2.tres")
const WIZARD_CHARACTER_STATS_3 = preload("res://resources/wizard_character_stats_3.tres")
@onready var title = %Title
@onready var description = %Description
@onready var character_portrait = %CharacterPortrait

@export var run_stats : RunStats
var current_character :CharacterStats :set = set_current_character
func _ready():
	set_current_character(WARRIOR_CHARACTER_STATS_2)
	
func set_current_character(new_character: CharacterStats):
	current_character = new_character
	title.text = current_character.name
	description.text = current_character.description
	character_portrait.texture = current_character.portrait

func _on_start_button_pressed():
	run_stats.character_stats = current_character
	get_tree().change_scene_to_packed(FARM_SCENE)




func _on_assasin_button_pressed():
	set_current_character(ROGUE_CHARACTER_STATS)

func _on_wizard_button_pressed():
	set_current_character(WIZARD_CHARACTER_STATS_3)

func _on_warrior_button_pressed():
	set_current_character(WARRIOR_CHARACTER_STATS_2)
