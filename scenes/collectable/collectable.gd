extends Area2D
const FLOWER = preload("res://scenes/collectable/flower.tscn")

func _on_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("interact"):
		EventBus.spawn_something.emit(FLOWER, position)
