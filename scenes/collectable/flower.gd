class_name Flower
extends Area2D

func _on_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("interact"):
		EventBus.something_clicked.emit(self, "Collect")

