extends Sprite2D

func _process(_delta):		
	if EventBus.mouse_over > 0:
		hide()
	else:
		show()
	var cursor_position = get_global_mouse_position()
	var x_position = roundi((cursor_position.x -8) / 16) * 16
	var y_position = roundi((cursor_position.y -8)/ 16) * 16
	cursor_position = Vector2(x_position,y_position)
	global_position = cursor_position
