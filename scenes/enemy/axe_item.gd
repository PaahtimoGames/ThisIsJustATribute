class_name AxeItem
extends Area2D

@export var item_name := "Sword"	
		
func _on_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("interact"):
		EventBus.something_clicked.emit(self, "Attack")
