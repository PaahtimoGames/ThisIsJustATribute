class_name Barrel
extends Area2D

const WHITE_SPRITE_MATERIAL = preload("res://art/white_sprite_material.tres")
const GORE_SPLAT_L_GROUND_01_MONO = preload("res://art/GORE_Splat_L_Ground_01_mono.wav")
const GORE_STAB_01_MONO = preload("res://art/GORE_Stab_01_mono.wav")
const BLOOD_EFFECT = preload("res://scenes/particles/blood_effect.tscn")
const COLLECTABLE = preload("res://scenes/collectable/collectable.tscn")

@export var health := 3
@onready var ray_cast_2d = $RayCast2D
@onready var sprite_2d = $Sprite2D

@export var evasion = 18
@export var max_turns := 2
@export var target_range := 10
@export var threashhold_1 = 1
@export var threashhold_2 = 4	
@export var threashhold_3 = 6
var target : Player
var tween : Tween
var enemy_took_action := 1

@onready var alert = $Alert

func _ready():
	alert.hide()
	enemy_took_action = max_turns

func reset_turns():
	enemy_took_action = max_turns
	
	
func set_target(new_target : Player):
	target = new_target
	
func take_damage(hit : int, damage : int):
	tween = create_tween()
	tween.tween_callback(Shaker.shake.bind(self,16,0.15))
	tween.tween_interval(0.17)
	
	if hit < evasion:
		var text = "Enemy: Hit missed. %s  %s" % [hit, evasion]
		EventBus.text_block_added.emit(text)
		return

	
	var damage_number = 0
	if damage >= threashhold_3:
		damage_number = 3
	elif  damage >= threashhold_2:
		damage_number = 2
	elif  damage >= threashhold_1:
		damage_number = 1
	health -= damage_number
	
	var text2 = "Enemy: Took damage %s" % [damage_number]
	EventBus.text_block_added.emit(text2)
		
	if health > 0:
		SfxPlayer.play(GORE_STAB_01_MONO)
	else:
		SfxPlayer.play(GORE_SPLAT_L_GROUND_01_MONO)
		EventBus.spawn_something.emit(COLLECTABLE,position)
		
		
	sprite_2d.material = WHITE_SPRITE_MATERIAL

	tween.finished.connect(
		func():
			sprite_2d.material = null
			if health <= 0:
				queue_free()
	)


func is_dead() -> bool:
	return health <= 0	
	
		
func _on_input_event(_viewport, event, _shape_idx):
	if event.is_action_pressed("interact"):
		EventBus.something_clicked.emit(self, "Attack")
