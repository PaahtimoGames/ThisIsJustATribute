class_name Enemy
extends Node2D

const WHITE_SPRITE_MATERIAL = preload("res://art/white_sprite_material.tres")
const GORE_SPLAT_L_GROUND_01_MONO = preload("res://art/GORE_Splat_L_Ground_01_mono.wav")
const GORE_STAB_01_MONO = preload("res://art/GORE_Stab_01_mono.wav")
const BLOOD_EFFECT = preload("res://scenes/particles/blood_effect.tscn")
const COLLECTABLE = preload("res://scenes/collectable/collectable.tscn")
const AXE_ITEM = preload("res://scenes/enemy/axe_item.tscn")

@export var move_distance := 16
@export var health := 3
@onready var ray_cast_2d = $RayCast2D
@onready var sprite_2d = $Sprite2D

@export var evasion = 18
@export var max_turns := 2
@export var target_range := 10
@export var threashhold_1 = 1
@export var threashhold_2 = 4	
@export var threashhold_3 = 6
var target : Player
var tween : Tween
var enemy_took_action := 1

@onready var alert = $Alert

var turn_timer = 2
var turn_timer_counter = 0
func _ready():
	alert.hide()
	enemy_took_action = max_turns

func _process(delta):
	turn_timer_counter -= delta
	if turn_timer_counter <= 0:
		update()
		turn_timer_counter = turn_timer
func reset_turns():
	enemy_took_action = max_turns
			
func set_target(new_target : Player):
	target = new_target
	
func take_damage(hit : int, damage : int):
	tween = create_tween()
	tween.tween_callback(Shaker.shake.bind(self,16,0.15))
	tween.tween_interval(0.17)
	
	if hit < evasion:
		var text = "Enemy: Hit missed. %s  %s" % [hit, evasion]
		EventBus.text_block_added.emit(text)
		return

	
	var damage_number = 0
	if damage >= threashhold_3:
		damage_number = 3
	elif  damage >= threashhold_2:
		damage_number = 2
	elif  damage >= threashhold_1:
		damage_number = 1
	health -= damage_number
	
	var text2 = "Enemy: Took damage %s" % [damage_number]
	EventBus.text_block_added.emit(text2)
		
	if health > 0:
		SfxPlayer.play(GORE_STAB_01_MONO)
	else:
		SfxPlayer.play(GORE_SPLAT_L_GROUND_01_MONO)
		spawn_particle()
		
		var random = randi_range(0,10)
		
		if random > 8:
			EventBus.spawn_something.emit(AXE_ITEM,position)
		else:
			EventBus.spawn_something.emit(COLLECTABLE,position)
		
		
	sprite_2d.material = WHITE_SPRITE_MATERIAL

	tween.finished.connect(
		func():
			sprite_2d.material = null
			if health <= 0:
				queue_free()
	)


func is_dead() -> bool:
	return health <= 0	
	
func spawn_particle():
	EventBus.spawn_something.emit(BLOOD_EFFECT,position)
	
		
func update():		
	var direction = Vector2.ZERO

	var range_to_target = 0
	if target:
		range_to_target = (position.distance_to(target.position)) /32
		
	if target and range_to_target <= target_range:
		alert.show()
		direction = move_to_target()
	else:
		alert.hide()
		direction = random_movement()
		
	if ray_cast_2d.is_colliding():
		var raytarget = ray_cast_2d.get_collider()
		if raytarget and raytarget.is_in_group("player"):
			var hit = randi_range(1,20)
			var damage = randi_range(1,6)
			raytarget.take_damage(hit, damage) 
		
		if raytarget != self:
			enemy_took_action -= max_turns
			ray_cast_2d.enabled = false
			return

	ray_cast_2d.enabled = false
	enemy_took_action -= 1
	position += direction

func move_to_target()-> Vector2:
	var direction = Vector2.ZERO
				
	if target.position.x > position.x:
		direction.x += move_distance
		set_raycast_direction(Vector2(8,8), Vector2(move_distance,0))
	elif target.position.x < position.x:
		direction.x -= move_distance
		set_raycast_direction(Vector2(8,8), Vector2(-move_distance,0))
	elif target.position.y < position.y:
		direction.y -= move_distance
		set_raycast_direction(Vector2(8,8), Vector2(0,-move_distance))
	elif target.position.y > position.y:
		direction.y += move_distance
		set_raycast_direction(Vector2(8,8), Vector2(0,move_distance))
		
	return direction
	
func random_movement() -> Vector2:
	var direction = Vector2.ZERO
	var input = randi_range(0,3)
	
	if input == 0:
		direction.x += move_distance
		set_raycast_direction(Vector2(8,8), Vector2(move_distance,0))
	elif input == 1:
		direction.x -= move_distance
		set_raycast_direction(Vector2(8,8), Vector2(-move_distance,0))
	elif input == 2:
		direction.y -= move_distance
		set_raycast_direction(Vector2(8,8), Vector2(0,-move_distance))
	elif input == 3:
		direction.y += move_distance
		set_raycast_direction(Vector2(8,8), Vector2(0,move_distance))

	return direction
		
func set_raycast_direction(ray_position: Vector2,direction : Vector2):
	ray_cast_2d.target_position = direction
	ray_cast_2d.position = ray_position
	ray_cast_2d.force_raycast_update()
	ray_cast_2d.enabled = true


