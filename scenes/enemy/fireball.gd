class_name Fireball
extends Area2D

@onready var ray_cast_2d = $RayCast2D

var tween : Tween
var caster : Player

func set_player(player : Player):
	caster = player
	
func _process(_delta):				
	if ray_cast_2d.is_colliding():
		var raytarget = ray_cast_2d.get_collider()
		if raytarget and raytarget.is_in_group("enemy"):
			var enemy = raytarget as Enemy
			attack_enemy(enemy) 


func attack_enemy(target : Enemy):
	var roll = DungeonMaster.roll_hope_and_fear()
	var roll_number = roll[0]
	var type = roll[1]
	var damage = randi_range(1,6)

	var text = ""
	if type < 0:
		text = "Player attacked %s with Fear." % roll_number
	elif type > 0:
		text = "Player attacked %s with Hope." % roll_number 
		caster.hope +=1
	else:
		text = "Player attacked with critical success."
		roll_number = 24
		caster.hope +=1
		
	caster.hope_updated.emit()
	EventBus.text_block_added.emit(text)
	
	target.take_damage(roll_number, damage) 
	caster.player_took_action -= caster.max_turns
	queue_free()
