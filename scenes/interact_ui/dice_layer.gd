extends CanvasLayer

@onready var hope_text = %HopeText
@onready var fear_text = %FearText


func _ready():
	hide()
	hope_text.text = ""
	fear_text.text = ""
	EventBus.roll_hope_and_fear_dice.connect(roll_create_rolls)

	
func roll_create_rolls(hope_roll : int, fear_roll : int):
	show()
	hope_text.text = str(hope_roll)
	fear_text.text = str(fear_roll)
	
	get_tree().create_timer(1,false).timeout.connect(
		func():
			hide()
	)
