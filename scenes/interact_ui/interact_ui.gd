extends CanvasLayer
@onready var attack = $TextureRect/VBoxContainer/Attack
var current_target : Node2D

func _ready():
	EventBus.something_clicked.connect(show_ui)



func _on_attack_button_down():
	if current_target:
		EventBus.attack_event.emit(current_target)

func show_ui(node : Node2D, text : String):
	show()
	current_target = node
	attack.text = text
