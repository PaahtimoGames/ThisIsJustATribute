class_name InventoryUI
extends Control
const ITEM_BASE = preload("res://scenes/itemdbn/item_base.tscn")
@onready var invetory_base = $InvetoryBase
@onready var grid_bag = $GridBag
@onready var equipment_slots = $EquipmentSlots

@export var inventory_component : InventoryComponent
@export var parent_node : Node
var item_held = null
var item_offset = Vector2.ZERO
var last_container = null
var last_pos = Vector2.ZERO

func _ready():
	inventory_component.pickup_item_signal.connect(pickup_item)


func _process(_delta):
	var cursor_position = get_global_mouse_position()
	if Input.is_action_just_pressed("inventory_grap"):
		grap(cursor_position)
	if Input.is_action_just_released("inventory_grap"):
		release(cursor_position)
	if Input.is_action_just_pressed("uninteract"):
		use_item(cursor_position)
	if item_held != null:
		item_held.global_position = cursor_position + item_offset
		
func use_item(cursor_ps):
	var c = get_container_under_cursor(cursor_ps)
	if c != null and c.has_method("grab_item"):
		var item_in_use = c.grab_item(cursor_ps) as InventoryItem
		if item_in_use != null:
			item_in_use.use_item(parent_node)
		
func grap(cursor_ps):
	var c = get_container_under_cursor(cursor_ps)
	if c != null and c.has_method("grab_item"):
		item_held = c.grab_item(cursor_ps)
		if item_held != null:
			last_container = c
			last_pos = item_held.global_position
			item_offset = item_held.global_position - cursor_ps
			move_child(item_held,get_child_count())
			
func release(cursor_pos):
	if item_held == null:
		return
	var c = get_container_under_cursor(cursor_pos)
	if c == null:
		drop_item()
	elif c.has_method("insert_item"):
		if c.insert_item(item_held):
			item_held = null
		else:
			return_item_to_last_slot()
	else:
		return_item_to_last_slot()
	
func drop_item():
	item_held.queue_free()
	item_held = null
		
func return_item():
	item_held.global_position = last_pos
	last_container.insert_item(item_held)
	item_held = null
		
func get_container_under_cursor(cursor_pos):
	var containers = [grid_bag,equipment_slots,invetory_base]
	for c in containers:
		if c.get_global_rect().has_point(cursor_pos):
			return c
	return null
	
func return_item_to_last_slot():
	item_held.global_position = last_pos
	last_container.insert_item(item_held)
	item_held = null
	
	
func pickup_item(item_id):
	var item = ITEM_BASE.instantiate()
	item.set_meta("id", item_id)
	item.texture = load(Itemdb.get_item(item_id)["asset"])
	add_child(item)
	if !grid_bag.insert_item_at_first_available_spot(item):
		item.queue_free()
		return false
	return true
	
	
