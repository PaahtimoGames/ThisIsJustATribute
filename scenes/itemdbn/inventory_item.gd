class_name InventoryItem
extends TextureRect

@export var item_name := "Name"
@export var actions :Array[PGAction]

func use_item(parent_node : Node):
	for pgaction in actions:
		var action = pgaction.duplicate()
		action.init(parent_node)
		action.enter()

	queue_free()
