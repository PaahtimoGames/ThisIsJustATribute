extends Node
const ICON_PATH = "res://test_objects/"
const ITEMS = {
	"berry": {
		"asset": ICON_PATH + "berry_bush/berry.png",
		"slot": "NONE"
	},
	"plant": {
		"asset": ICON_PATH + "test_plant/plant_grown.png",
		"slot": "NONE"
	},
	"wood": {
		"asset": ICON_PATH + "tree/wood.png",
		"slot": "NONE"
	},
	"coin": {
		"asset": ICON_PATH + "tree/wood.png",
		"slot": "NONE"
	},
	"error": {
		"asset": ICON_PATH + "potion_3.png",
		"slot": "NONE"
	},
}

func get_item(item_id):
	if item_id in ITEMS:
		return ITEMS[item_id]
	else:
		return ITEMS["error"]

