extends Node

@onready var start_button = %StartButton
@onready var exit_button = %ExitButton


func _on_start_button_pressed():
	SceneManager.load_character_selector()
	
func _on_exit_button_pressed():
	get_tree().quit()
