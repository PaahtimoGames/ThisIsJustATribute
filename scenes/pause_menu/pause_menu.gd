extends Control
@onready var paused := false

func _process(_delta):
	if Input.is_action_just_pressed("esc") and not paused:
		pause()
	elif Input.is_action_just_pressed("esc") and paused:
		unpause()
		
func pause():
	show()
	get_tree().paused = true
	paused = true
	
func unpause():
	hide()
	get_tree().paused = false
	paused = false
	
func _on_return_button_pressed():
	unpause()


func _on_quit_button_pressed():
	get_tree().quit()
