extends Node2D

@export var player : NodeContainer
var movement_component : MovementComponent

func _ready():
	for child_node in player.get_children():
		if child_node is MovementComponent:
			movement_component = child_node
			
func _process(_delta):
	queue_redraw()
	
func _draw():				
	if movement_component.current_path.size() <= 1:
		return
		
	var clone_array = movement_component.current_path.duplicate()
	for i in range(0, clone_array.size()):
		if movement_component.current_path_index > i:
			continue
		var node_position = Vector2i(clone_array[i].x *16,clone_array[i].y *16) + Vector2i(8,8)		
		draw_circle(node_position,1,Color.WHITE_SMOKE)
	#draw_polyline(clone_array,Color.RED,1)
