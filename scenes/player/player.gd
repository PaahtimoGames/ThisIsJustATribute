class_name Player
extends NodeContainer

signal health_updated
signal hope_updated

const WHITE_SPRITE_MATERIAL = preload("res://art/white_sprite_material.tres")
const GORE_SPLAT_L_GROUND_01_MONO = preload("res://art/GORE_Splat_L_Ground_01_mono.wav")
const GORE_STAB_01_MONO = preload("res://art/GORE_Stab_01_mono.wav")
@onready var ray_cast_2d = $RayCast2D
@export var max_turns := 2

@onready var sprite_2d = $Sprite
@onready var movement_component = $MovementComponent

@export var health := 10
var hope := 0 : set = set_hope
@export var evasion := 10
@export var threashhold_1 := 1
@export var threashhold_2 := 4	
@export var threashhold_3 := 6

@export var abilities : Array[PlayerAbility] = []

var player_took_action := 2
var tween : Tween
@export var target_range := 1.5
var new_position := Vector2.ZERO

var target : Player
func set_hope(value : int):
	hope = value
	hope_updated.emit()
		
func move_to_target()-> Vector2:
	var direction = Vector2.ZERO
				
	if target.position.x > position.x:
		direction.x += 16
		set_raycast_direction(Vector2(8,8), Vector2(16,0))
	elif target.position.x < position.x:
		direction.x -= 16
		set_raycast_direction(Vector2(8,8), Vector2(-16,0))
	elif target.position.y < position.y:
		direction.y -= 16
		set_raycast_direction(Vector2(8,8), Vector2(0,-16))
	elif target.position.y > position.y:
		direction.y += 16
		set_raycast_direction(Vector2(8,8), Vector2(0,16))
		
	return direction
	
	
func _ready():
	new_position = position
	player_took_action = max_turns
	movement_component.tilemap = tilemap
	movement_component.get_ready()
	EventBus.player = self
	
func _draw():
	if movement_component.current_path.is_empty():
		return
	draw_polyline(movement_component.current_path,Color.RED)
	
func take_damage(hit : int, damage : int):
	
	tween = create_tween()
	tween.tween_callback(Shaker.shake.bind(self,16,0.15))
	tween.tween_interval(0.17)
	
	if hit < evasion:
		var text2 = "Player: Hit missed. %s  %s" % [hit, evasion]
		EventBus.text_block_added.emit(text2)
		return

	
	var damage_number = 0
	if damage >= threashhold_3:
		damage_number = 3
	elif  damage >= threashhold_2:
		damage_number = 2
	elif  damage >= threashhold_1:
		damage_number = 1
	health -= damage_number
	health_updated.emit()
	
	var text = "Player: Took damage %s" % [damage_number]
	EventBus.text_block_added.emit(text)
	if health > 0:
		SfxPlayer.play(GORE_STAB_01_MONO)
	else:
		SfxPlayer.play(GORE_SPLAT_L_GROUND_01_MONO)
	sprite_2d.material = WHITE_SPRITE_MATERIAL

	tween.finished.connect(
		func():
			sprite_2d.material = null
			if health <= 0:
				tween.kill()
				SceneManager.load_mainmenu()
	)

	
func attack_event_called(node : Node):
	var range_to_target = 0
	var enemy_target = node as Enemy
	if node:
		range_to_target = (position.distance_to(node.position)) /16
		
	if enemy_target and enemy_target.is_in_group("enemy"):
		attack_enemy(range_to_target, enemy_target)
	var barrel_target = node as Barrel
	
	if barrel_target and barrel_target.is_in_group("enemy"):
		attack_barrel(range_to_target, barrel_target)
		
	var item_target = node as AxeItem
	if item_target and item_target.is_in_group("enemy"):
		var is_not_in_range = range_to_target > target_range
		if is_not_in_range:
			var text = "Player: Enemy out of range"
			EventBus.text_block_added.emit(text)
			return
		EventBus.pickup_item.emit(item_target.item_name)
		node.queue_free()
		
	if node and node.is_in_group("coin"):
		var is_not_in_range = range_to_target > target_range
		if is_not_in_range:
			var text = "Player: Enemy out of range"
			EventBus.text_block_added.emit(text)
			return
		EventBus.pickup_item.emit("coin")
		node.queue_free()
		
	if node and node.is_in_group("Flower"):
		var is_not_in_range = range_to_target > target_range
		if is_not_in_range:
			var text = "Player: Enemy out of range"
			EventBus.text_block_added.emit(text)
			return
		EventBus.pickup_item.emit("coin")
		node.queue_free()
		
func attack_barrel(range_to_target : float, barrel : Barrel):
	var is_not_in_range = range_to_target > target_range
	var text := ""
	if is_not_in_range:
		text = "Player: Enemy out of range"
		EventBus.text_block_added.emit(text)
		return
	var roll = DungeonMaster.roll_hope_and_fear()
	var roll_number = roll[0]
	var type = roll[1]
	var damage = randi_range(1,6)


	if type < 0:
		text = "Player attacked %s with Fear." % roll_number
	elif type > 0:
		text = "Player attacked %s with Hope." % roll_number 
		hope +=1
	else:
		text = "Player attacked with critical success."
		roll_number = 24
		hope +=1
		
	EventBus.text_block_added.emit(text)
	
	barrel.take_damage(roll_number, damage) 
	player_took_action -= max_turns
	
func attack_enemy(range_to_target : float, enemy_target : Enemy):
	var is_not_in_range = range_to_target > target_range
	var text := ""
	if is_not_in_range:
		text = "Player: Enemy out of range"
		EventBus.text_block_added.emit(text)
		return
	var roll = DungeonMaster.roll_hope_and_fear()
	var roll_number = roll[0]
	var type = roll[1]
	var damage = randi_range(1,6)


	if type < 0:
		text = "Player attacked %s with Fear." % roll_number
	elif type > 0:
		text = "Player attacked %s with Hope." % roll_number 
		hope +=1
	else:
		text = "Player attacked with critical success."
		roll_number = 24
		hope +=1
		
	hope_updated.emit()
	EventBus.text_block_added.emit(text)
	
	enemy_target.take_damage(roll_number, damage) 
	player_took_action -= max_turns
		
func _process(_delta):	
	var direction = Vector2.ZERO
		
		
	if target:
		direction = move_to_target()
	else:
		direction = move_with_input()

			
	if ray_cast_2d.is_colliding():
		var raytarget = ray_cast_2d.get_collider()
		if raytarget and raytarget.is_in_group("enemy"):
			EventBus.something_clicked.emit(raytarget, "Attack")
		if target:
			player_took_action -= max_turns
		return
	elif direction != Vector2.ZERO:
		player_took_action -= 1
		
	position = position + direction
			
	ray_cast_2d.enabled = false

func move_with_input() -> Vector2:
	var direction = Vector2.ZERO
	if Input.is_action_just_pressed("right"):
		direction.x += 16
		set_raycast_direction(Vector2(8,8), Vector2(16,0))
	elif Input.is_action_just_pressed("left"):
		direction.x -= 16
		set_raycast_direction(Vector2(8,8), Vector2(-16,0))
	elif Input.is_action_just_pressed("up"):
		direction.y -= 16
		set_raycast_direction(Vector2(8,8), Vector2(0,-16))
	elif Input.is_action_just_pressed("down"):
		direction.y += 16
		set_raycast_direction(Vector2(8,8), Vector2(0,16))
		
	return direction
	
func reset_turn():
	player_took_action = max_turns
	
		
func set_raycast_direction(ray_position: Vector2,direction : Vector2):
	ray_cast_2d.target_position = direction
	ray_cast_2d.position = ray_position
	ray_cast_2d.force_raycast_update()
	ray_cast_2d.enabled = true
