extends Control


var index := 0
var player : Player
@export var character_icon : TextureRect
@export var button : Button
@export var progress_bar :ProgressBar
@export var character_name : Label
@export var hope_bar : ProgressBar


func update_data(new_index : int, char_name : String, player_data : Player):
	character_name.text = char_name
	index = new_index
	character_icon.texture = player_data.sprite_2d.texture
	player = player_data
	progress_bar.max_value = player.health
	progress_bar.value = player.health	
	player.health_updated.connect(update_slider_data)
	player.hope_updated.connect(update_hope_label)
	hope_bar.max_value = 5
	hope_bar.value = 0
func update_slider_data():
	progress_bar.value = player.health
	
func update_hope_label():
	hope_bar.value = player.hope
	
func _on_button_pressed():
	EventBus.player_button_clicked.emit(index)
