extends RichTextLabel


func _ready():
	text = ""
	EventBus.text_block_added.connect(text_updated)
	
func text_updated(new_text : String):
	text = "%s\n%s" % [new_text, text]
