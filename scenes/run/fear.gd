extends Label

var fear := 0
func _ready():
	EventBus.fear_updated.connect(health_update)
	
func health_update(value : int):
	fear += value
	text = "Fear: %s" % fear
