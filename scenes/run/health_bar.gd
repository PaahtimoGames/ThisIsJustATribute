extends Label


func _ready():
	EventBus.health_updated.connect(health_update)
	
func health_update(value : int):
	text = "Health: %s" % value
