extends Label
@export var max_hope := 5
var hope := 0
func _ready():
	EventBus.hope_updated.connect(health_update)
	
func health_update(value : int):
	hope += value
	if hope > max_hope:
		hope = max_hope
	text = "Hope: %s" % hope
