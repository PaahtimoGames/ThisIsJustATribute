extends Node
@onready var player_ui_list = $CanvasLayer2/PlayerUIList
const TILE_0004 = preload("res://art/new_sprites/tile_0084.png")
const TILE_0005 = preload("res://art/new_sprites/tile_0085.png")
const TILE_0007 = preload("res://art/new_sprites/tile_0087.png")
const TILE_0008 = preload("res://art/new_sprites/tile_0088.png")
const MOUSE_CURSOR = preload("res://art/mouse_cursor.png")
const BLOOD = preload("res://art/new_sprites/tile_0133.png")
@onready var camera = $Camera
const PLAYER_PREFAB = preload("res://scenes/player/player.tscn")
const PLAYER_WIZARD = preload("res://scenes/player/player_wizard.tscn")
const LARGE_ENEMY = preload("res://scenes/enemy/large_enemy.tscn")
const ENEMY_PREFAB = preload("res://scenes/enemy/enemy.tscn")
const ENEMY_PREFAB_2 = preload("res://scenes/enemy/enemy_2.tscn")
const BLOOD_EFFECT = preload("res://scenes/particles/blood_effect.tscn")
@onready var ability_bar = $CanvasLayer2/AbilityBar
const ENEMY_HARDER = preload("res://scenes/enemy/enemy_harder.tscn")
var player : Player
@onready var cursor = $Cursor
@onready var interact_ui = $InteractUI
@onready var follow_button = $CanvasLayer2/FollowButton
const PLAYER_UI = preload("res://scenes/player/player_ui.tscn")
const FIREBALL = preload("res://scenes/enemy/fireball.tscn")
@onready var inventory = $CanvasLayer2/Inventory
@onready var tile_map = $TileMap

@onready var enemy_spawns = $EnemyList
var enemy_list : Array
const SPEED = 5000
var frame_counter := 0

var player_list :Array
var current_player_index := 0

var following := true
var fireball_ready := false
var inventory_enabled = false

func _ready():
	disable_inventory()
	for child in player_ui_list.get_children():
		child.queue_free()
		
	create_player(160,32,PLAYER_PREFAB)
	#create_player(160,64,PLAYER_WIZARD)

	EventBus.something_clicked.connect(show_interact_ui)
	EventBus.attack_event.connect(attack_event_called)
	EventBus.spawn_something.connect(spawn_something)
	EventBus.player_button_clicked.connect(player_button_pressed)
	EventBus.fireball_pressed.connect(fireball_activated)
	EventBus.pickup_item.connect(pickup_item)
	update_current_player()
	for enemy_positon in enemy_spawns.get_children():		
		spawn_enemy(enemy_positon.position)
func pickup_item(item_name: String):
	inventory.pickup_item(item_name)
	
func fireball_activated():
	if player.hope <= 0:
		return
	fireball_ready = true
	cursor.texture = BLOOD
	
func fireball_deactivated():
	fireball_ready = false
	cursor.texture = MOUSE_CURSOR
	
func player_button_pressed(index :int):
	current_player_index = index
	update_current_player()
	un_follow()
	
	if player.player_took_action <= 0:
		reset_enemies()

func spawn_something(node : Object, spawn_position : Vector2):
	var coin = node.instantiate()
	coin.position = spawn_position
	add_child(coin)

func create_player(position_x: int, position_y:int, player_asset : PackedScene):
	player = player_asset.instantiate()
	player.position.x = position_x
	player.position.y = position_y
	add_child(player)

	player_list.append(player)
	if not player.is_inside_tree():
		await player.ready
				
	var player_ui = PLAYER_UI.instantiate()

	var	index = player_list.size() -1
	var player_name = "Player %s" % index
	player_ui.update_data(index,player_name, player)
	player_ui_list.add_child(player_ui)

func attack_event_called(node : Node):
	player.attack_event_called(node)
	
func show_interact_ui(node : Node2D, text : String):
	interact_ui.show_ui(node,text)
	
func clear_nulls():
	for enemy in enemy_list:
		if ! is_instance_valid(enemy):
			enemy_list.erase(enemy)
	
func enable_inventory():
	inventory.set_process(true)
	inventory.show()
	inventory_enabled = true
	
func disable_inventory():
	inventory.set_process(false)
	inventory.hide()
	inventory_enabled = false
	
func _process(_delta):	
	if Input.is_action_just_pressed("show_menu") and not inventory_enabled:
		enable_inventory()
	elif Input.is_action_just_pressed("show_menu") and inventory_enabled:
		disable_inventory()
		
	if not interact_ui.current_target:
		interact_ui.hide()

	var cursor_position = camera.get_global_mouse_position()
	var x_position = roundi((cursor_position.x -8) / 16) * 16
	var y_position = roundi((cursor_position.y -8)/ 16) * 16
	cursor_position = Vector2(x_position,y_position)
	cursor.global_position = cursor_position

	if Input.is_action_just_pressed("interact") and fireball_ready:

		var fireball = FIREBALL.instantiate()
		fireball.position = cursor_position
		fireball.set_player(player)
		player.hope -= 1
		add_child(fireball)
		fireball_deactivated()
	if Input.is_action_pressed("uninteract"):
		interact_ui.hide()
		fireball_deactivated()
	clear_nulls()
	if not enemies_took_action():
		update_enemies()
		if enemies_took_action():
			reset_players()
			
	if frame_counter < 5:
		frame_counter += 1
		return
	frame_counter = 0
	if player.player_took_action > 0:
		player.update()

	if player.player_took_action <= 0:
		current_player_index += 1
		fireball_deactivated()
		if current_player_index >= player_list.size():
			current_player_index = 0
			
		if players_took_action():
			reset_enemies()
		else :
			update_current_player()

func hide_other_selected():
	for player_from_list : Player in player_list:
		player_from_list.selected_hide()
	
func reset_players():
	for player_from_list : Player in player_list:
		player_from_list.reset_turn()
		
	current_player_index = 0
	update_current_player()
	
func update_current_player():	
	player = player_list[current_player_index]
	hide_other_selected()
	player.selected_show()	
	if not player.target:
		camera.set_target(player)
		
	ability_bar.create_containers(player)
	
func players_took_action() -> bool:
	for player_from_list : Player in player_list:			
		if player_from_list.player_took_action > 0:
			return false
	return true

func reset_enemies():
	for enemy in enemy_list:
		enemy.reset_turns()

	
func update_enemies():
	for enemy : Enemy in enemy_list:
		if enemy.enemy_took_action > 0:
			enemy.update()
		
func enemies_took_action() -> bool:
	for enemy in enemy_list:				
		if enemy.enemy_took_action > 0:
			return false
	return true
	
func spawn_enemy(enemy_position : Vector2):
	var random = randi_range(0,10)
	var enemy : Enemy
	if random < 6:
		enemy = ENEMY_PREFAB_2.instantiate() as Enemy
	else:
		enemy = ENEMY_HARDER.instantiate() as Enemy
	enemy.position = enemy_position
	
	var x_position = roundi(enemy.position.x / 16) * 16
	var y_position = roundi(enemy.position.y / 16) * 16
	enemy.position = Vector2(x_position,y_position)
	
	enemy.set_target(player)
	enemy_list.push_front(enemy)
	add_child(enemy)


func _on_end_turn_button_pressed():
	player.player_took_action = 0


func _on_follow_button_pressed():
	if following:
		un_follow()
	else :
		follow_button.text = "Unfollow"
		for play in player_list:
			play.target = player
			
		player.target = null
	following = !following


func un_follow():
	follow_button.text = "Follow"
	for play in player_list:
		play.target = null
	following = false
		
