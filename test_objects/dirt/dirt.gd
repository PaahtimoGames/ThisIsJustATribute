class_name Dirt
extends Node2D

@onready var interactable_component = %InteractableComponent


func disable_interaction():
	interactable_component.interactable = false
	
func enable_interaction():
	interactable_component.interactable = true
	
