extends Node2D


func _ready():
	EventBus.spawn_something.connect(spawn_something)


func spawn_something(node : Object, spawn_position : Vector2, function):
	var coin = node.instantiate()
	coin.position = spawn_position
	function.call(coin)
	add_child(coin)
