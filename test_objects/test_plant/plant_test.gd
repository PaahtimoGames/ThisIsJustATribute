class_name Plant
extends Node2D
@onready var sprite_2d = %Sprite2D
@export var new_sprite : Texture
@onready var interactable_component = %InteractableComponent

var dirt : Dirt

# Called when the node enters the scene tree for the first time.
func _ready():
	await get_tree().create_timer(2).timeout
	sprite_2d.texture = new_sprite



