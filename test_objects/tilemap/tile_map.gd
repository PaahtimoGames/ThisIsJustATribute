class_name PGTilemap
extends TileMap

const TREE = preload("res://test_objects/tree/tree.tscn")

@export var player : NodeContainer
var astar_grid : AStarGrid2D
var sizex = 16
var sizey = 16
	
var width = 20
var height = 20

var tree_tilemap_size : Rect2i
func _ready():
	EventBus.tile_made_walkable.connect(tile_made_walkable)
	astar_grid = AStarGrid2D.new()
	astar_grid.region = get_used_rect()
	astar_grid.cell_size = Vector2(16,16)
	astar_grid.diagonal_mode = AStarGrid2D.DIAGONAL_MODE_AT_LEAST_ONE_WALKABLE
	astar_grid.update()

	tree_tilemap_size = get_used_rect()
		
	var rect = get_used_rect()
	for x in rect.size.x:
		for y in rect.size.y:
			var tile_position = Vector2i(x + rect.position.x,y + rect.position.y)
			var tile_data = get_cell_tile_data(0,tile_position)
			if tile_data == null or !tile_data.get_custom_data("walkable"):
				astar_grid.set_point_solid(tile_position)				

func _process(_delta):
	generate_tile(player.position)

func tile_made_walkable(pos :Vector2,value : bool):
	var current_position_in_grid_x = roundi((pos.x) / 16)
	var current_position_in_grid_Y = roundi((pos.y)/ 16)
	var position_on_grid = Vector2(current_position_in_grid_x,current_position_in_grid_Y)
	if value:
		astar_grid.set_point_solid(position_on_grid,false)
	else:
		astar_grid.set_point_solid(position_on_grid)

func generate_tile(target_position : Vector2):
	var draw_position = Vector2(target_position.x - (width*16)/2.0, target_position.y- (height*16)/2.0)
	var tile_pos = local_to_map(draw_position)
	for x in range(width):
		for y in range(height):
			var tile_position = Vector2i(x + tile_pos.x,y + tile_pos.y)
			
			var tile_data = get_cell_tile_data(1,tile_position)
			if tile_data != null:
				var node = TREE.instantiate()
				node.position = Vector2(tile_position.x * sizex, tile_position.y * sizey)
				add_child(node)
				set_cell(1,tile_position,-1)
				

				

